> THIS PROJECT MOVED TO [git.ant.lgbt](https://git.ant.lgbt/ConfusedAnt/system-monitor-api)

<div align="center">
    <a href="https://gitlab.com/ConfusedAnt/system-monitor-api/">
        <img src="logo.png" height="80">
    </a>
    <h3> System Monitor API </h3>
    <div align="center">
        A simple Python REST API to show system stats.
        <br>
        <br>
        [View Website](https://ant.lgbt/posts/developement/system-monitor-api/)
        •
        [Report Bug](https://ant.lgbt/posts/developement/system-monitor-api//-/issues/new)
        •
        [Request Feature](https://ant.lgbt/posts/developement/system-monitor-api//-/issues/new)
    </div>
</div>

## Table of Contents
* [About the Project](#about-the-project)
* [Installation](#installation)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

## About the Project
It creates a “REST”-API, and gives back JSON values that you set before, in order to check on the system remotely (e.g. through a Website/Dashboard). 
You can see it in action, by going to the [About-Page](https://ant.lgbt/about/), or the [Hosting-Page](https://ant.lgbt/hosting/) on my website.

### Installation
First, install the requirements.

```bash
pip install -r requirements.txt
```
After that is complete, run the script.py file.

## License
This project is licensed under the [MIT License](https://choosealicense.com/licenses/bsl-1.0/)

## Contact
* Email: [confused@ant.lgbt](mailto:confused@ant.lgbt)
* Reddit: [u/1Yanik3](https://www.reddit.com/user/1Yanik3)
* Gitlab: [Yanik Ammann](https://gitlab.com/ConfusedAnt)
* Discord: Yanik#0704