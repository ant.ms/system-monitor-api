from flask import Flask
from flask_restful import Resource, Api
import psutil
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
api = Api(app)

class cpuUsage(Resource):
    def get(self):
        return psutil.cpu_percent()

class memoryUsage(Resource):
    # https://psutil.readthedocs.io/en/latest/
    def get(self):
        return {
            "percentage": psutil.virtual_memory().percent,
            "total": psutil.virtual_memory().total,
            "available": psutil.virtual_memory().available,
            "used": psutil.virtual_memory().used,
            "free": psutil.virtual_memory().free
        }

class storageUsageRoot(Resource):
    def get(self):
        return {
            "percentage": psutil.disk_usage('/').percent,
            "total": psutil.disk_usage('/').total,
            "used": psutil.disk_usage('/').used,
            "free": psutil.disk_usage('/').free
        }

class storageUsageMnt(Resource):
    def get(self):
        return {
            "percentage": psutil.disk_usage('/mnt').percent,
            "total": psutil.disk_usage('/mnt').total,
            "used": psutil.disk_usage('/mnt').used,
            "free": psutil.disk_usage('/mnt').free
        }

api.add_resource(cpuUsage, '/cpu')
api.add_resource(memoryUsage, '/ram')
api.add_resource(storageUsageRoot, '/storage/root')
api.add_resource(storageUsageMnt, '/storage/mnt')

if __name__ == '__main__':
     app.run(port='5002', host='0.0.0.0')